# Analyse Stereo Calibrations

This small script shows the stereo projection errors of a stero calibration according to matching checkerboard images.

## Usage

In the Example is an example checkerboard_collection and a ROS yaml Stereocalibration.
`python3 stereoCalibrationAnalyse.py Examples/2020_10_08_30xCorners.csv Examples/2020_07_29/`

## What happens behind

A [ROS stereo calibration](https://docs.ros.org/en/melodic/api/sensor_msgs/html/msg/CameraInfo.html) consists of
 * camera intrinsics (focal length towards x and y and camera center for x and y)
 * camera distortion parameters
 * R rectification matrix, rotation matrix to adjust epipolar lines
 * projection matrix, 4x3 Matrix with the translation part T, gives the distance between left and right camera
 relatively to the new projection matrix focal length

The transformation from left to right camera can be calculated using the rectification and projection matrix.
`Trans_l2r = transpose(R_left) * T_right * R_right`


From the csv file we get a set of found checkerboard corners.
The columns of the csv are:
 * x, y corner coordinates in left image 
 * x, y corner coordinates in right image 
 * x, y, Z corner coordinate in object coordinates 
 * id of matching checkerboard
In our example a checkerboard has 187 (17x11) matching corners.


For every checkerboard (187 corresponding corners) the script 
_Finds an object pose from 3D-2D point correspondences._ using `opencv solvePnP` for left and right camera

The script combines the `Trans_l2r` with the found left camera position, so each found corner can be projected
to the right image using Right Distortion and Kamera intrinsics.

Now the script calculates the difference between the projected corner and the actual found corners in the right image.

In the charts every dots stands for one projected corner error distance. 
