#!/usr/bin/env python3

# Analye Stereo Calibrations
# Copyright (C) 2020 Peter Klausing (peter_klausing@agdsn.me)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import cv2 as cv
import yaml
import sys
import pandas
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import argparse


class CalibrationAnalyzer:
    def __init__(self, frame_file_path, config_folder, ):
        self.configFolder = config_folder
        self.frameFilePath = frame_file_path
        self.cameraInfoLeft = None
        self.cameraInfoRight = None
        self.cam_left = None
        self.cam_right = None
        self.dist_left = None
        self.dist_right = None
        self.rect_left = None
        self.rect_right = None
        self.rect_left_hom = None
        self.rect_right_hom = None
        self.proj_left = None
        self.proj_right = None
        self.pts = None
        self.errorsR2LDf = None
        self.errorsL2RDf = None
        self.errorTransformations = []

    def load_data(self):
        with open(self.configFolder + "left.yaml", 'r') as cameraInfoLeftFile, \
                open(self.configFolder + "right.yaml", 'r') as cameraInfoRightFile:
            try:
                self.cameraInfoLeft = yaml.safe_load(cameraInfoLeftFile)
                self.cameraInfoRight = yaml.safe_load(cameraInfoRightFile)
            except yaml.YAMLError as exc:
                print(exc)
                sys.exit("yaml error loading files")

        self.cam_left = np.array(self.cameraInfoLeft["camera_matrix"]["data"])
        self.cam_left = self.cam_left.reshape((3, 3))
        self.dist_left = np.array(self.cameraInfoLeft["distortion_coefficients"]["data"])
        self.cam_right = np.array(self.cameraInfoRight["camera_matrix"]["data"])
        self.cam_right = self.cam_right.reshape((3, 3))
        self.dist_right = np.array(self.cameraInfoRight["distortion_coefficients"]["data"])

        self.rect_left = np.array(self.cameraInfoLeft["rectification_matrix"]["data"])
        self.rect_left = self.rect_left.reshape((3, 3))
        self.rect_left_hom = np.eye(4)
        self.rect_left_hom[:3, :3] = self.rect_left
        self.rect_right = np.array(self.cameraInfoRight["rectification_matrix"]["data"])
        self.rect_right = self.rect_right.reshape((3, 3))
        self.rect_right_hom = np.eye(4)
        self.rect_right_hom[:3, :3] = self.rect_right

        self.proj_right = np.array(self.cameraInfoRight["projection_matrix"]["data"])
        self.proj_right = self.proj_right.reshape((3, 4))
        names = ["c_left_img_x", "c_left_img_y",
                 "c_right_img_x", "c_right_img_y",
                 "obj_x", "obj_y", "obj_z",
                 "frame_id"]
        self.pts = pandas.read_csv(self.frameFilePath, sep=',', names=names)

    def calc_errors(self):
        frames = self.pts.groupby("frame_id")
        errors_r2l = []
        errors_l2r = []
        for frame in frames:
            objpts = frame[1][["obj_x", "obj_y", "obj_z"]].to_numpy()
            corners2_left = frame[1][["c_left_img_x", "c_left_img_y"]].to_numpy()
            corners2_right = frame[1][["c_right_img_x", "c_right_img_y"]].to_numpy()

            ret_left, rvecs_left, tvecs_left = cv.solvePnP(
                objpts, corners2_left, self.cam_left, self.dist_left)
            ret_right, rvecs_right, tvecs_right = cv.solvePnP(
                objpts, corners2_right, self.cam_right, self.dist_right)

            # Turn "rotation vector" into rotation matrix:
            rMat3_left = np.zeros((3, 3), float)
            cv.Rodrigues(rvecs_left, rMat3_left)
            rMat3_right = np.zeros((3, 3), float)
            cv.Rodrigues(rvecs_right, rMat3_right)

            rMat4_left = np.eye(4)
            rMat4_left[0:3, 0:3] = rMat3_left
            rMat4_right = np.eye(4)
            rMat4_right[0:3, 0:3] = rMat3_right

            transform_left_mat = np.empty((4, 4))
            transform_left_mat[:3, :3] = rMat3_left
            transform_left_mat[:3, 3] = np.transpose(tvecs_left)
            transform_left_mat[3, :] = [0, 0, 0, 1]
            transform_right_mat = np.empty((4, 4))
            transform_right_mat[:3, :3] = rMat3_right
            transform_right_mat[:3, 3] = np.transpose(tvecs_right)
            transform_right_mat[3, :] = [0, 0, 0, 1]
            dist_left_to_right = self.proj_right[0, 3] / self.proj_right[0, 0]
            translation_right = np.eye(4)
            translation_right[0, 3] = dist_left_to_right
            # calculate transformation from left to right from calibration
            l2r_mat = \
                np.matmul(np.matmul(np.transpose(self.rect_right_hom), translation_right), self.rect_left_hom)
            # there is probably a better methode
            r2l_mat = np.linalg.inv(l2r_mat)

            l2r_mat = np.matmul(transform_left_mat, l2r_mat)
            rvecs_l2r = l2r_mat[:3, :3]
            tvecs_l2r = l2r_mat[:3, 3]

            r2l_mat = np.matmul(r2l_mat, transform_left_mat, )
            rvecs_r2l = r2l_mat[:3, :3]
            tvecs_r2l = r2l_mat[:3, 3]

            # draw reprojected corners form right image recognized to left image
            # and vice versa
            cornerspts_r2l, jcb = cv.projectPoints(objpts, rvecs_r2l, tvecs_r2l, self.cam_left, self.dist_left)
            cornerspts_l2r, jcb = cv.projectPoints(objpts, rvecs_l2r, tvecs_l2r, self.cam_right, self.dist_right)

            for (corner_left_correct, corner_left_proj, corner_right_correct, corner_right_proj, objpt) in \
                    zip(corners2_left, cornerspts_r2l, corners2_right, cornerspts_l2r, objpts):
                objpt4 = np.insert(objpt, 3, 1)

                error_left = corner_left_correct - corner_left_proj
                # because image index goes from top to bottom
                error_vec_left = np.array([error_left.item(0), -error_left.item(1)])
                error_vec_length_left = np.linalg.norm(error_vec_left)
                pt_to_cam_vec_left = np.matmul(transform_left_mat, objpt4.transpose())
                pt_to_cam_len_left = np.linalg.norm(pt_to_cam_vec_left[0:3])
                errors_r2l.append((
                    corner_right_correct.item(0), corner_right_correct.item(1),
                    error_vec_left.item(0), error_vec_left.item(1),
                    error_vec_length_left, pt_to_cam_len_left))

                error_right = corner_right_correct - corner_right_proj
                # because image index goes from top to bottom
                error_vec_right = np.array([error_right.item(0), -error_right.item(1)])
                error_vec_length_right = np.linalg.norm(error_vec_right)
                pt_to_cam_vec_right = np.matmul(transform_right_mat, objpt4)
                pt_to_cam_len_right = np.linalg.norm(pt_to_cam_vec_right[0:3])
                errors_l2r.append((
                    corner_left_correct.item(0), corner_left_correct.item(1),
                    error_vec_right.item(0), error_vec_right.item(1),
                    error_vec_length_right, pt_to_cam_len_right))

        column_names = ["x", "y", "error_x", "error_y", "error_length", "cam_dist"]
        self.errorsR2LDf = pandas.DataFrame(errors_r2l, columns=column_names)
        # print(self.errorsR2LDf)
        self.errorsL2RDf = pandas.DataFrame(errors_l2r, columns=column_names)

    def visualize_erros(self, direction):
        f, ((axx, axy), (axxy, ax_cam_dist)) = plt.subplots(2, 2)

        errors = self.errorsR2LDf if direction else self.errorsL2RDf
        f.suptitle("Project {}".format("Right to Left" if direction else "Left to Right"))
        axx.scatter(errors["x"], errors["error_length"], s=1)
        axx.set_xlabel("x")
        axx.set_ylabel("error length px")
        axx.set_ylim(0)

        axy.scatter(errors["y"], errors["error_length"], s=1)
        axy.set_xlabel("y")
        axy.set_ylabel("error length px")
        axy.set_ylim(0)

        minima = 0
        maxima = 100

        norm = colors.Normalize(vmin=minima, vmax=maxima, clip=True)
        ylrdmapper = cm.ScalarMappable(norm=norm, cmap=cm.YlOrRd)
        errorsColors = list(map(ylrdmapper.to_rgba, errors["error_length"]))
        axxy.scatter(errors["x"], errors["y"],
                     s=1,
                     c=errorsColors)
        plt.colorbar(ylrdmapper, ax=axxy)
        axxy.set_xlabel("x", )
        axxy.set_ylabel("y", )
        axxy.set_ylim(0)
        axxy.set_xlim(0)

        minima = 0
        maxima = 2000

        norm = colors.Normalize(vmin=minima, vmax=maxima, clip=True)
        prismmapper = cm.ScalarMappable(norm=norm, cmap=cm.prism)

        errorsColors = list(map(prismmapper.to_rgba, self.errorsR2LDf["x"]))

        ax_cam_dist.scatter(errors["cam_dist"], errors["error_length"],
                            s=1,
                            c=errorsColors)
        ax_cam_dist.set_xlabel("distance to camera")
        ax_cam_dist.set_ylabel("error length")
        ax_cam_dist.set_ylim(0)
        plt.colorbar(prismmapper, ax=ax_cam_dist)


if __name__ == "__main__":
    # load file
    parser = argparse.ArgumentParser()
    parser.add_argument("config_folder")
    parser.add_argument("frame_file_path")
    args = parser.parse_args()
    ca = CalibrationAnalyzer(args.config_folder, args.frame_file_path)
    ca.load_data()
    ca.calc_errors()
    ca.visualize_erros(True)
    ca.visualize_erros(False)
    plt.show()
